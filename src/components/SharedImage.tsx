export function SharedImage({ src }: { src: string }) {
    return <div className="bg-slate-200 my-2 p-2 text-sm text-neutral-600 font-semibold rounded-sm">
        <img alt="shared-image" src={src} className="max-w-[600px] max-h-[600px]" />
    </div>
}