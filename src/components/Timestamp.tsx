export function Timestamp({ children }: { children?: React.ReactNode }) {
    return <div className="bg-neutral-100 my-2 py-2 px-4 text-sm text-neutral-600 font-semibold rounded-full shadow-sm">
        {children}
    </div>
}