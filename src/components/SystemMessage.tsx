export function SystemMessage({ children }: { children?: React.ReactNode }) {
    return <div className="bg-neutral-100 my-2 py-2 px-4 text-sm text-neutral-400 font-semibold italic rounded-sm">
        {children}
    </div>
}