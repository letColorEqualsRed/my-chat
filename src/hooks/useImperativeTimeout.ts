import { useRef, useEffect, useCallback } from "react";

export function useImperativeTimeout(callback: (() => void), delay: number) {
    const timeoutId = useRef<NodeJS.Timeout | null>(null);
    const savedCallback = useRef<() => void>();

    // Remember the latest callback.
    useEffect(() => {
        savedCallback.current = callback;
    }, [callback]);

    // this handle clears the timeout
    const clear = useCallback(() => {
        if (timeoutId.current)
            clearTimeout(timeoutId.current);
    }, []);
    // this handle sets our timeout
    const set = useCallback(() => {
        // but clears the old one first
        clear();
        timeoutId.current = setTimeout(() => {
            if (savedCallback.current)
                savedCallback.current();
        }, delay);
    }, [delay, clear]);

    // also, clear the timeout on unmount
    useEffect(() => clear, [clear]);

    return { set, clear };
}