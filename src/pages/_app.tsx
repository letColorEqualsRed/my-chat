import { type AppType } from "next/dist/shared/lib/utils";
import React, { useState } from "react";

import "~/styles/globals.css";

export const StubAuthContext = React.createContext({
  isLoggedIn: false,
  setIsLoggedIn: (isLoggedIn: boolean) => {
    console.log(isLoggedIn)
  }
})

const MyApp: AppType = ({ Component, pageProps }) => {
  const [isLoggedIn, setIsLoggedIn] = useState(false)
  return <StubAuthContext.Provider value={{ isLoggedIn, setIsLoggedIn }}>
    <Component {...pageProps} />;
  </StubAuthContext.Provider>
};

export default MyApp;
