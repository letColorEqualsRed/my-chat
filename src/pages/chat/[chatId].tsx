import { type NextPage } from "next";
import Link from "next/link";
import { useContext, useEffect, useRef } from "react";
import classNames from 'classnames';
import { useRouter } from "next/router";
import { StubAuthContext } from "../_app";
import { Timestamp } from "~/components/Timestamp";
import { SharedImage } from "~/components/SharedImage";
import { SystemMessage } from "~/components/SystemMessage";

import chatrooms from "../../../messages.json"

type SpecialMessageType = "ts" | "image"

interface Chatroom {
    displayName: string
    messages: {
        from: string;
        content: string;
        meta?: {
            type?: SpecialMessageType
            imageSrc?: string
        }
    }[]
}

const Chatroom: NextPage = () => {
    const router = useRouter();
    const chatInd = parseInt(router.query.chatId as string);
    const chatroom = chatrooms[chatInd]

    const { isLoggedIn } = useContext(StubAuthContext);

    const chatRef = useRef<HTMLDivElement>(null);

    useEffect(() => {
        if (!!router && !isLoggedIn) {
            router.push('/').catch(console.error)
        }
    }, [router, isLoggedIn])

    useEffect(() => {
        if (!!router && !chatroom) {
            router.push('/chat/0').catch(console.error)
        }
    }, [router, chatroom])

    useEffect(() => {
        chatRef.current?.scroll({
            top: chatRef.current.scrollHeight
        })
    }, [chatInd])

    return <main className={classNames("min-h-screen flex flex-col items-center justify-center bg-neutral-300")}>
        {
            isLoggedIn &&
            <div className="flex flex-col h-[680px] w-[1440px] border border-gray-300 shadow-lg">
                <header className="flex flex-row bg-neutral-100 w-full h-24 border-b border-gray-300">
                    <div className="w-96 border-r border-gray-300" />
                    <div className="flex flex-row flex-1 items-center gap-x-4 p-6 text-xl font-semibold text-neutral-900">
                        <div className="h-10 w-10 bg-green-100 rounded-full" />
                        {chatroom?.displayName}
                    </div>
                </header>
                <div className="flex flex-1 w-full max-h-[680px] flex-row">
                    <ul data-chatroom-list=""
                        className="h-full w-96 border-r border-solid border-r-gray-300 bg-neutral-100">
                        {chatrooms.map((chatroom, ind) => {
                            const isCurrentChat = ind == chatInd
                            return <Link href={`/chat/${ind}`} key={ind}>
                                <li className={classNames("w-full p-6 text-lg font-semibold border-b border-neutral-300",
                                    { "bg-white text-neutral-900": isCurrentChat },
                                    { "bg-neutral-100 hover:bg-neutral-50 text-neutral-700 opacity-80 hover:opacity-100": !isCurrentChat }
                                )}>
                                    {chatroom.displayName}
                                </li>
                            </Link>
                        })}
                    </ul>
                    <div data-current-chatroom-ind={chatInd}
                        data-current-chatroom-name={chatroom?.displayName}
                        ref={chatRef}
                        className="flex flex-col flex-1 shadow-inner bg-neutral-50 overflow-y-scroll">
                        <div className="flex flex-row px-6 mb-4 justify-center">
                            <SystemMessage>Use MyChat on your phone to see older messages.</SystemMessage>
                        </div>
                        {chatroom?.messages.map(({ from, content, meta }, ind) => {
                            return (
                                <div key={ind} className={classNames("flex flex-row px-6 last:mb-6",
                                    { "justify-end": from === 'self' },
                                    { "justify-center": from === 'system' })}>
                                    {meta?.type === 'ts'
                                        ? <Timestamp>{content}</Timestamp>
                                        : (meta?.type === 'image' && !!meta.imageSrc)
                                            ? <SharedImage src={meta.imageSrc} />
                                            : <div className={classNames("max-w-3/5 mb-2 px-4 py-2 rounded shadow-sm",
                                                { "bg-slate-200": from !== 'self' && from !== 'system' },
                                                { "bg-slate-100": from === 'self' })}>
                                                {content.split("\n").map(line => (
                                                    <>{line}<br /></>
                                                ))}
                                            </div>}
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
        }
    </main>
}

export default Chatroom