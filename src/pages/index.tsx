import { type NextPage } from "next";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { useContext, useState } from "react";
import classNames from 'classnames';
import { useImperativeTimeout } from "~/hooks/useImperativeTimeout";
import { StubAuthContext } from "./_app";

/**
 * Hint: 1687 5 2
 * Luke 12:15
 */
const PASSWORD = 'abundance'

const Login: NextPage = () => {
  const router = useRouter();

  const [password, setPassword] = useState('')
  const [showPasswordIncorrect, setShowPasswordIncorrect] = useState(false);

  const [isLoggingIn, setIsLoggingIn] = useState(false)

  const { isLoggedIn, setIsLoggedIn } = useContext(StubAuthContext)

  const passwordCorrect = useImperativeTimeout(() => {
    setIsLoggedIn(true)
    setIsLoggingIn(false)
    router.push("chat/0").catch(console.error)
  }, 2000)

  const passwordIncorrect = useImperativeTimeout(() => {
    setIsLoggingIn(false)
    setShowPasswordIncorrect(true)
    setTimeout(() => {
      setShowPasswordIncorrect(false)
    }, 2000)
  }, 500)

  const onSubmit: React.FormEventHandler<HTMLFormElement> = (evt) => {
    evt.preventDefault();

    setIsLoggingIn(true)

    if (password === PASSWORD) {
      passwordCorrect.set()
    } else {
      passwordIncorrect.set()
    }
  }
  return (
    <>
      <Head>
        <title>MyChat - The Free Instant Messaging Service</title>
        <meta name="description" content="Created by Isaac Tsui" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={classNames("h-screen flex flex-col items-center justify-center bg-neutral-100", { "cursor-progress": isLoggingIn })}>
        <form className="group flex flex-col items-center justify-center h-4/5 w-4/5 bg-white border border-solid border-gray-300 rounded-lg shadow-lg transition-all hover:shadow-xl focus-within:shadow-xl"
          onSubmit={onSubmit}>
          <div className="h-72 w-72 mb-6 border border-solid border-gray-300 rounded-full shadow-inner bg-contain bg-center bg-no-repeat bg-blend-soft-light"
            style={{
              backgroundImage: 'url("/muipropic.png")'
            }} />

          <div className="h-8 w-72 relative flex flex-row mb-4">
            <h2 className={classNames("absolute inset-0 text-center text-xl font-semibold transition-all duration-500 ease-in-out", { "-translate-x-1/2 opacity-0": isLoggedIn })}>
              Please enter your password:
            </h2>
            <h2 className={classNames("absolute inset-0 text-center text-xl font-semibold transition-all duration-500 ease-in-out", { "translate-x-1/4 opacity-0": !isLoggedIn })}>
              Welcome back, mui1972!
            </h2>
          </div>

          <input className={classNames("h-12 w-72 pl-2 border border-solid border-gray-300 shadow-inner focus:shadow-none focus-visible:outline-blue-300 transition-all",
            { "!border-red-500": showPasswordIncorrect })}
            type="password"
            name="password"
            disabled={isLoggingIn}
            value={password}
            onChange={evt => setPassword(evt.target.value)} />
        </form>
      </main>
    </>
  );
};

export default Login;
